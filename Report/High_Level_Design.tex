\chapter{High Level Design}\label{Chap: High_level_design}

%This is what I did to test and confirm my hypothesis.


%You may want to split this chapter into sub chapters depending on your design. I suggest you change the title to something more specific to your project.

%This is where you describe your design process in detail, from component/device selection to actual design implementation, to how you tested your system. Remember detail is important in technical writing. Do not just write I used a computer give the computer specifications or the oscilloscopes part number. Describe the system in enough detail so that someone else can replicate your design as well as your testing methodology.

%If you use or design code for your system, represent it as flow diagrams in text.
As mentioned previously, this project has two main aspects: an  HDL and RISC-V (with high-level language) implementation of an FFT on an audio stream. This chapter details the necessary subsystems for both implementations as well as the requirements and specifications for the design to meet the objectives of this investigation.

\section{Subsystems} \label{sec: HLD Subsytems} 
There are 3 main hardware subsystems in this project. These are the FPGA, the ADC and the Amp-Mic subsystems. An audio signal is provided into the mic, which is connected to an ADC. The ADC converts the audio stream into a digital signal that can be processed by the FPGA. The FPGA is configured to implement an FFT on the signal provided by the FPGA. Figure \ref{fig: subsystems} shows this interaction between the subsystems.

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 1\textwidth]{../Figures/subsystems}
		\caption{Interactions between the subsystems used in this project}
		\label{fig: subsystems}
	\end{centering}
\end{figure}


\subsection{FPGA}\label{subsec: HDL FPGA}
The FPGA used in this project is Intel's Cyclone IV E FPGA, which is on the DE0-Nano development board, Figure \ref{fig: DE0_nano} \cite{Altera2012}. The first consideration for choosing an FPGA, was the ability for running a Linux-capable RISC-V core. From the research described in the Literature Review \ref{Chap: Lit_ Review} \ref{subsec: Linux_on_RV_on_FPGA} , the Cyclone IV was one of the FPGAs listed as being capable of running VexRiscv\cite{LinunxOnLitex}. The DE0-Nano is an FPGA development board that was within the budget of the experiment, includes 22 320 Logic elements, an on-board ADC, 153 GPIO pins which make for easy interfacing with other peripherals, and enough on board power. This makes this board an ideal choice for this application.


\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 0.5\textwidth]{../Figures/DE0_NANO.jpg}
		\caption{The DE0- Nano Board}
		\label{fig: DE0_nano}
	\end{centering}
\end{figure}


\subsection{Analog-to-Digital Converter - ADC}\label{subsec: HDL ADC}
The ADC found on the DE0-Nano is the ADC128S022, an 8 channel, 12 bit ADC with throughput rates of 50 - 200 kSPS \cite{ADCInstruments2013}. Although this ADC can only read data from one channel at a time, its resolution and speed is sufficient for this application. A diagram of the ADC is shown in Figure \ref{fig: ADC} and a description of the important pins can be found in table \ref{tbl: ADC_pins}

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 0.35\textwidth]{../Figures/ADC.jpg}
		\caption{Schematic of the ADC128S022- the on board ADC}
		\label{fig: ADC}
	\end{centering}
\end{figure}

\begin{table}[h]
	\resizebox{\textwidth}{!}{%
		\begin{tabular}{|l|l|}
			\hline 
			\textbf{Pin Symbol} & \textbf{Description} \\ \hline \hline
			IN0 - IN7 			& Analog inputs \\ \hline
			SCLK 				& Digital clock input - performance range is from 0.8 - 3.2 MHz \\ \hline
			DOUT 				& Digital data output - data samples are output from this pin on the falling edge of SCLK \\ \hline
			DIN 				& Digital data input - on the rising edge of SCLK control registered is loaded to this pin \\ \hline
			CS					& Chip select - while this pin is low, a conversion process takes place \\ \hline
		\end{tabular}%
	}
\caption{Description of the pins on the ADC}\label{tbl: ADC_pins}
\end{table}


\subsection{Microphone}\label{subsec: HDL Mic}
For audio input into the system, a microphone is required. Because of their easy of use implementation, low cost and good audio quality, an electret microphone was chosen as the audio input. Electret microphones however, provide a minute input signal and therefore require an amplifier so that the signal can be utilised. Due to the current world climate, it is not possible utilise the on-campus facilities to build an amp. Therefore, although more expensive, a mic-amp combination IC is the appropriate choice in this scenario. The MAX9814, seen in Figure \ref{fig: mic_amp} \cite{MAX9814} is the IC of choice. As with most other mic-amp IC combinations, the MAX9814 is easy to use with simple power supply voltage of 3.3V - 5.5V, GND and audio output pins. What sets this mic apart from other mics in a similar price range is its Automatic Gain Control (AGC). AGC prevent clipping at the output by reducing the gain at the input. This feature as well as the audio quality of the electret mic, make it suitable for this application as well as others such as Bluetooth headsets, high-quality portable recorders and entertainment systems \cite{MAX9814}.

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 0.2\textwidth]{../Figures/MAX9814.jpg}
		\caption{The mic-amp IC \cite{max_amp_adafruit}}
		\label{fig: mic_amp}
	\end{centering}
\end{figure}

\section{Emulation}
Both the HDL and RISC-V implementations make use of the subsystems mentioned above. Before the RISC-V implementation is completed however, it is good practice to utilise an emulation. This emulation will produce results that can be compared to both the FFT implementations in hardware. The most popular and suitable choice of software for this emulation is Qemu \cite{qemu}. As in the FPGA RISC-V implementation, the FFT code can be compiled on the host PC and be transferred to the RISC-V implementation. A generated audio stream, with known values can also be transferred and the FFT performed on that input. The results obtained can transferred back to the host PC for comparison. This is illustrated in Figure \ref{fig: qemu plan}

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 1\textwidth]{../Figures/qemu_subsystem}
		\caption{Interactions Between the Subsystems for the RISC-V Emulation}
		\label{fig:  qemu plan}
	\end{centering}
\end{figure}


\section{Requirements and Specifications}\label{sec: HDL requirements}
The requirements for this project can be broken down into two sections: HDL implementation Requirements (HR)  and the RISC-V implementation Requirements (RR).

\begin{table}[h!]
	\centering{%
		\begin{tabular}{|l|l|}
			\hline 
			\textbf{Requirement} & \textbf{Description} \\ \hline \hline
			\textbf{HR1}					& Connection of ADC to mic-amp IC \\ \hline
			\textbf{HR2} 				& Reading in Audio signal from ADC \\ \hline
			\textbf{HR3} 				& Implement working FFT code in HDL  \\ \hline
			\textbf{HR4} 				& Implement chosen FFT on Audio signal from ADC \\ \hline
			\textbf{HR5} 				& Measure resources consumed \\ \hline
			\textbf{HR6} 				& Measure time taken for FFT to be implemented \\ \hline
		\end{tabular}%
	}
	\caption{HDL implementation Requirements (HR) }\label{tbl: HDL proj req}
\end{table}


\begin{table}[h!]
	\centering{%
		\begin{tabular}{|l|l|}
			\hline 
			\textbf{Requirement} & \textbf{Description} \\ \hline \hline
			\textbf{RR1}					& Implement FFT on audio in C\\ \hline
			\textbf{RR2} 				& Implement Linux  on capable RISC-V core in emulation (Qemu) \cite{qemu}\\ \hline
			\textbf{RR3} 				& Cross-compile FFT code and execute in emulation \\ \hline
			\textbf{RR4} 				& Measure results and resources consumed\\ \hline
			\textbf{RR5} 				& Implement Linux capable RISC-V core on FPGA \\ \hline
			\textbf{RR6} 				& Implement Linux on chosen core \\ \hline
			\textbf{RR7} 				& Implement FFT in Linux, on chosen core, on FPGA  \\ \hline
			\textbf{RR8} 				& Implement FFT on audio stream \\ \hline
			\textbf{RR9} 				& Measure results and resources consumed  \\ \hline
		\end{tabular}%
	}
	\caption{RISC-V implementation Requirements (RR) }\label{tbl: RISCV proj req}
\end{table}

Based on the requirements the following technical specifications have been drawn up. The specifications for the HDL implementation are shown in table \ref{tbl: HDL specs} and the specifications for the RISC-V are shown in table \ref{tbl: rv specs}

\begin{table}[h]
\makebox[\linewidth]{%
	\begin{tabular}{|l|l|p{3.4in}|}
	\hline 
	\textbf{Specification} &\textbf{Based on Requirement} & \textbf{Description} \\ \hline \hline
	\textbf{HS1}		&	HR1		& Confirm that the mic-amp system is powered and is registering audio \\ \hline
	\textbf{HS2} 	&	HR2		& Read in Audio signal between ranges of 20 Hz - 20kHz \\ \hline
	\textbf{HS3}		&	HR3		& Look up various existing FFT codes and choose an efficient version with simple implementation\\ \hline
	\textbf{HS4}		&	HR4		& Implement 1024-point FFT in HDL on Audio signal from ADC  \\ \hline
	\textbf{HS5}		&	HR5 	& Utilise FPGA utilisation formula \ref{eqn: FPGA_utilisation} to measure FPGA power consumption \\ \hline
	\textbf{HS6}		&	HR6		& Measure time taken for FFT to be compiled, programmed and executed on device as well as Ease of Implementation \\ \hline
	\end{tabular}%
}
\caption{HDL technical specifications to meet requirements (HS) }\label{tbl: HDL specs}
\end{table}

\begin{table}[h!]
\makebox[\linewidth]{%
\begin{tabular}{|l|l|p{3.4in}|}
	\hline 
	\textbf{Specification} &\textbf{Based on Requirement} & \textbf{Description} \\ \hline \hline
	\textbf{RS1}		&	RR1		& Read in audio signal (20 Hz - 20kHz) and implement RISC-V Linux capable 1024-point  FFT on it\\ \hline
	\textbf{RS2} 	&	RR2		& Setup Qemu with virtual network for communication and emulate a Linux OS  on a RISC-V core\\ \hline
	\textbf{RS3}		&	RR3		& Use cross-compiler to compile FFT code for chosen FFT core and execute in emulation  \\ \hline
	\textbf{RS4}		&	RR4		& Measure accuracy of FFT  and resources consumed \\ \hline
	\textbf{RS5}		&	RR5		& Look for easy-to-implement and Linux capable RISC-V core and load onto FPGA using Quartus  \\ \hline
	\textbf{RS6}	&	RR6 	& Flash a Linux image on the chosen  core - usually done through serial communication \\ \hline
	\textbf{RS7}	&	RR7 	& Implement cross-compiled, 1024-point FFT in Linux \\ \hline
	\textbf{RS8}	&	RR8 	& Read in audio signal from memory and implement FFT on that. Do the same after reading audio signal from on board ADC \\ \hline
	\textbf{RS9}		&	RR9		& Measure time taken for FFT to be compiled, programmed and executed on device as well as Ease of Implementation  \\ \hline
\end{tabular}%
	}
	\caption{RISC-V technical specifications to meet requirements (RS) }\label{tbl: rv specs}
\end{table}




