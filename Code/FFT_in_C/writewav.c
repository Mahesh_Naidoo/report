// writewav.c - Create a wav file by piping raw samples to ffmpeg
// Written by Ted Burke - last updated 10-2-2017
//
// To compile:
//
//    gcc writewav.c -o writewav -lm
//
 
#include <stdio.h>
#include <stdint.h>
#include <math.h>
 
#define N 44100
 
void main()
{
    // Create audio buffer
    int16_t buf[N] = {0}; // buffer
    int n;                // buffer index
    double Fs = 44100.0;  // sampling frequency
     
    // Generate 1 second of audio data - it's just a 1 kHz sine wave
    for (n=0 ; n<N ; ++n) buf[n] = 6* sin(n*1000.0*2.0*M_PI/Fs); //M_PI  is just the pi constant //to make sound louder increase amplitude
     
    // Pipe the audio data to ffmpeg, which writes it to a wav file
    FILE *pipeout;
    pipeout = popen("ffmpeg -y -f s16le -ar 44100 -ac 1 -i - beep.wav", "w");
    fwrite(buf, 2, N, pipeout);
    pclose(pipeout);
}