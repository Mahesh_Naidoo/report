//https://batchloaf.wordpress.com/2017/02/10/a-simple-way-to-read-and-write-audio-and-video-files-in-c-using-ffmpeg/
/*The following example reads 20 ms of samples from the beginning of a WAV file called whistle.wav 
 *then prints the sample values to a CSV file. The WAV file format is 16-bit signed integer samples,
 * mono, with a sampling frequency of 44.1 kHz.
 * FIRST INSTALL FFmepg: sudo apt-get install ffmpeg
 */


//
// readwav.c - Read samples from a WAV file using FFmpeg via a pipe
// Written by Ted Burke - last updated 10-2-2017
//
// To compile:
//
//    gcc readwav.c -o readwav -lm
//
 
#include <stdio.h>
#include <stdint.h>
 
#define N 16 //882
 
void main()
{
    // Create a 20 ms audio buffer (assuming Fs = 44.1 kHz)
    int16_t buf[N] = {0}; // buffer
    int n;                // buffer index
     
    // Open WAV file with FFmpeg and read raw samples via the pipe.
    FILE *pipein;
    pipein = popen("ffmpeg -i whistle.wav -f s16le -ac 1 -", "r");
    fread(buf, 2, N, pipein);
    pclose(pipein);
     
    // Print the sample values in the buffer to a CSV file
    FILE *csvfile;
    csvfile = fopen("samples.csv", "w");
    for (n=0 ; n<N ; ++n) fprintf(csvfile, "%d\n", buf[n]);
    fclose(csvfile);
}

/*
The first argument to the popen() function is a string containing the full FFmpeg command line. The specified options included in that command line control how FFmpeg will interpret the raw sample data it receives through the pipe and the type of file it will write.

    The "-y" option tells FFmpeg that it can overwrite the specified output file if it already exists.
    The "-f s16le" option tells FFmpeg that the format of the audio data it reads (from its standard input, which means via the pipe from our program) is raw PCM, signed integer, 16-bit and little-endian.
    The "-ar 44100" option tells FFmpeg that the sampling rate (i.e. sampling frequency) of the audio data it reads is 44.1 kHz.
    The "-ac 1" option tells FFmpeg that the number of channels in the audio data it reads is 1.
    The "-i -" option tells FFmpeg to read its input from standard input, which in this case means from the pipe, since FFmpeg was launched by popen.
    The second argument to popen is "r", which means that our program will read FFmpeg’s standard output via the pipe (it was the other way around in the previous example).
*/