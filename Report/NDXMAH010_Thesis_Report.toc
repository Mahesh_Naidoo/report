\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Background to the study}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Objectives of this Study}{2}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Problems to be Investigated}{2}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Purpose of the Study}{2}{subsection.1.2.2}% 
\contentsline {section}{\numberline {1.3}Scope and Limitations}{3}{section.1.3}% 
\contentsline {section}{\numberline {1.4}Plan of development}{4}{section.1.4}% 
\contentsline {chapter}{\numberline {2}Literature Review}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}RISC-V}{6}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}RISC-V Cores}{7}{subsection.2.1.1}% 
\contentsline {subsubsection}{RISC-V Naming Conventions}{7}{section*.6}% 
\contentsline {subsection}{\numberline {2.1.2}RISC-V on FPGA}{8}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Linux on RISC-V on FPGA}{9}{subsection.2.1.3}% 
\contentsline {section}{\numberline {2.2}Field Programmable Gate Arrays - FPGAs}{10}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Architecture of FPGAs}{11}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Optimisation the FFT on FPGA}{12}{subsection.2.2.2}% 
\contentsline {section}{\numberline {2.3}Fast Fourier Transform (FFT)}{13}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Algorithms to Calculate the FFT}{14}{subsection.2.3.1}% 
\contentsline {subsubsection}{Cooley-Tukey Algorithm (Radix-2)}{14}{section*.13}% 
\contentsline {subsubsection}{Decimation-in-Time (DIT), Decimation-in-Frequency (DIF) and Decimation-in-Time-Frequency (DITF) Algorithms}{15}{section*.14}% 
\contentsline {subsubsection}{Split-Radix FFT (SRFFT)}{16}{section*.16}% 
\contentsline {subsubsection}{Quick Fourier Transform (QFT)}{16}{section*.17}% 
\contentsline {subsubsection}{Fast Hartley Transform (FHT)}{17}{section*.18}% 
\contentsline {subsubsection}{Comparison FFT algorithms}{17}{section*.19}% 
\contentsline {subsection}{\numberline {2.3.2}Applications of the FFT}{18}{subsection.2.3.2}% 
\contentsline {chapter}{\numberline {3}Methodology}{19}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Project Plan}{19}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Part 1: Planning and Research}{20}{section.3.2}% 
\contentsline {section}{\numberline {3.3}Part 2: Literature Review}{20}{section.3.3}% 
\contentsline {section}{\numberline {3.4}Part 3: Experimentation}{20}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Part 3.1: Planning and research}{20}{subsection.3.4.1}% 
\contentsline {subsection}{\numberline {3.4.2}Part 3.2: Design, Implementation and Evaluation}{21}{subsection.3.4.2}% 
\contentsline {section}{\numberline {3.5}Part 3.3 and Part 4: Collection and Discussion of Results}{21}{section.3.5}% 
\contentsline {section}{\numberline {3.6}Part 5 and Part 6: Conclusions and Recommendations }{22}{section.3.6}% 
\contentsline {chapter}{\numberline {4}High Level Design}{23}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Subsystems}{23}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}FPGA}{24}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Analog-to-Digital Converter - ADC}{24}{subsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.1.3}Microphone}{25}{subsection.4.1.3}% 
\contentsline {section}{\numberline {4.2}Emulation}{26}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Requirements and Specifications}{26}{section.4.3}% 
\contentsline {chapter}{\numberline {5}Detailed Design}{29}{chapter.5}% 
\contentsline {section}{\numberline {5.1}HDL Design}{30}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Experiment 1 - HS1 - Mic-amp system}{30}{subsection.5.1.1}% 
\contentsline {subsubsection}{Experiment Setup}{30}{section*.35}% 
\contentsline {subsubsection}{Experiment Procedure and Expectations}{30}{section*.37}% 
\contentsline {subsection}{\numberline {5.1.2}Experiment 2 - HS2 - ADC system}{31}{subsection.5.1.2}% 
\contentsline {subsubsection}{Experiment Setup}{31}{section*.38}% 
\contentsline {subsubsection}{Experiment Procedure and Expectations}{31}{section*.40}% 
\contentsline {subsection}{\numberline {5.1.3}Experiment 3 - HS3 - FFT Codes}{32}{subsection.5.1.3}% 
\contentsline {subsection}{\numberline {5.1.4}Experiment 4 - HS4 - Implementing FFT}{33}{subsection.5.1.4}% 
\contentsline {subsubsection}{Experiment Setup}{33}{section*.42}% 
\contentsline {subsubsection}{Experiment Procedure and Expectations}{33}{section*.44}% 
\contentsline {subsection}{\numberline {5.1.5}Experiment 5 - HS5 - Measuring Power Consumption}{34}{subsection.5.1.5}% 
\contentsline {subsection}{\numberline {5.1.6}Experiment 6 - HS6 - Measuring the Speed and Ease of Implementation }{34}{subsection.5.1.6}% 
\contentsline {section}{\numberline {5.2}RISC-V Design}{35}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Experiment 1 - RS1 - Implementing FFT in C}{35}{subsection.5.2.1}% 
\contentsline {subsubsection}{Reading audio signal}{36}{section*.45}% 
\contentsline {subsubsection}{FFT in C}{36}{section*.47}% 
\contentsline {subsection}{\numberline {5.2.2}Experiment 2 - RS2 - Setup Qemu}{37}{subsection.5.2.2}% 
\contentsline {subsection}{\numberline {5.2.3}Experiment 3 - RS3 \& RS4 - Implementing FFT on Linux on RISC-V - Emulation}{38}{subsection.5.2.3}% 
\contentsline {subsection}{\numberline {5.2.4}Experiment 4 - RS5 \& RS6 - Implementing RISC-V Core and Linux on FPGA}{38}{subsection.5.2.4}% 
\contentsline {subsection}{\numberline {5.2.5}Experiment 5 - RS7 \& RS8 - Implementing FFT on Audio Stream on Linux, Vexriscv Core on FPGA}{39}{subsection.5.2.5}% 
\contentsline {subsection}{\numberline {5.2.6}Experiment 6 - RS9 - Resources Consumed and Ease of Implementation}{40}{subsection.5.2.6}% 
\contentsline {chapter}{\numberline {6}Results}{41}{chapter.6}% 
\contentsline {section}{\numberline {6.1}HDL Results}{41}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}Experiment 1 - HS1- Mic-amp System - Results}{41}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}Experiment 2 - HS2- ADC System - Results}{42}{subsection.6.1.2}% 
\contentsline {subsection}{\numberline {6.1.3}Experiments 3 \& 4- HS4 \& HS4- FFT Implementation - Results}{42}{subsection.6.1.3}% 
\contentsline {subsection}{\numberline {6.1.4}Experiments 5 - HS5 - Power Consumption - Results}{44}{subsection.6.1.4}% 
\contentsline {subsection}{\numberline {6.1.5}Experiments 6 - HS6 - Speed and Easy of Implementation - Results}{44}{subsection.6.1.5}% 
\contentsline {section}{\numberline {6.2}RISC-V Results}{45}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Experiment 1 - RS1- FFT in C - Results}{45}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}Experiment 2 - RS2 - Running Qemu - Results}{47}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}Experiment 3 - RS3 \& RS4 - Implementing FFT in Linux on Emulated RISC-V core - Results}{48}{subsection.6.2.3}% 
\contentsline {subsubsection}{Floating Point Implementation}{48}{section*.63}% 
\contentsline {subsubsection}{Integer FFT Implementation}{50}{section*.68}% 
\contentsline {subsection}{\numberline {6.2.4}Experiment 4 - RS5 \& RS6 - Running Vexriscv and Linux on FPGA - Results}{51}{subsection.6.2.4}% 
\contentsline {subsection}{\numberline {6.2.5}Experiment 5 - RS7 \& RS8 - Implementing FFT on Linux, on RISC-V on FPGA - Results}{52}{subsection.6.2.5}% 
\contentsline {subsection}{\numberline {6.2.6}Experiment 6 - RS9 - Speed, Resources Consumed and Ease of Implementation - Results}{52}{subsection.6.2.6}% 
\contentsline {subsubsection}{Speed}{53}{section*.76}% 
\contentsline {subsubsection}{Resources Consumed}{53}{section*.78}% 
\contentsline {subsubsection}{Ease of Implementation}{54}{section*.80}% 
\contentsline {chapter}{\numberline {7}Discussion}{56}{chapter.7}% 
\contentsline {section}{\numberline {7.1}Review of Linux Capable RISC-V cores}{56}{section.7.1}% 
\contentsline {section}{\numberline {7.2}FFT Implementation in HDL}{56}{section.7.2}% 
\contentsline {section}{\numberline {7.3}FFT Implementation in C (on Linux) on RISC-V Core - Qemu}{57}{section.7.3}% 
\contentsline {section}{\numberline {7.4}FFT Implementation in C (on Linux) on RISC-V Core - FPGA}{57}{section.7.4}% 
\contentsline {section}{\numberline {7.5}Comparison - HDL and RISC-V Implementations}{57}{section.7.5}% 
\contentsline {subsection}{\numberline {7.5.1}FPGA Resources Utilised}{58}{subsection.7.5.1}% 
\contentsline {subsection}{\numberline {7.5.2}Speed of Processing - Compilation}{58}{subsection.7.5.2}% 
\contentsline {subsection}{\numberline {7.5.3}Speed of Procecssing - Execution}{59}{subsection.7.5.3}% 
\contentsline {subsection}{\numberline {7.5.4}Ease of Implementation}{59}{subsection.7.5.4}% 
\contentsline {subsection}{\numberline {7.5.5}FFT Accuracy}{59}{subsection.7.5.5}% 
\contentsline {subsection}{\numberline {7.5.6}Potential to Improve in Further Iterations}{59}{subsection.7.5.6}% 
\contentsline {section}{\numberline {7.6}Using an ASIC RISC-V core}{60}{section.7.6}% 
\contentsline {chapter}{\numberline {8}Conclusions}{61}{chapter.8}% 
\contentsline {chapter}{\numberline {9}Recommendations}{62}{chapter.9}% 
\contentsline {chapter}{\numberline {A}Additional Figures, Code and Files}{68}{appendix.A}% 
\contentsline {section}{\numberline {A.1}Additional figures}{68}{section.A.1}% 
\contentsline {section}{\numberline {A.2}Additional Code}{73}{section.A.2}% 
\contentsline {subsection}{\numberline {A.2.1}Code for the ADC SPI Interface }{73}{subsection.A.2.1}% 
\contentsline {subsection}{\numberline {A.2.2}Code for implementing the FFT on ADC}{74}{subsection.A.2.2}% 
\contentsline {subsection}{\numberline {A.2.3}Code to Verify Frequency Read by ADC}{77}{subsection.A.2.3}% 
\contentsline {subsection}{\numberline {A.2.4}Code to Read in Wave File}{77}{subsection.A.2.4}% 
\contentsline {subsection}{\numberline {A.2.5}Code to implement FFT}{80}{subsection.A.2.5}% 
\contentsline {chapter}{\numberline {B}Addenda}{83}{appendix.B}% 
\contentsline {section}{\numberline {B.1}Ethics Form}{83}{section.B.1}% 
\contentsline {section}{\numberline {B.2}Bitbucket Link}{85}{section.B.2}% 
