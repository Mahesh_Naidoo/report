\chapter{Detailed Design} \label{Chap: Detailed_design}
This chapter details the experiments that have been performed to meet the requirements and specifications outlined in Section \ref{sec: HDL requirements}. \textbf{This chapter has been broken down into two main sections. The experiments and design first for the HDL implementation and then the same for the RISC-V implementation.}


One aspect of experimentation that is required in both the HDL and RISC-V implementations, is the generation of an audio signal. This is the signal that is played from the speaker or the audio signal that will be read in from memory. For this, Audacity - sound editing software-  can be used. All that is required is to set the type of audio tone, or signal to be \textit{sine}, and the required duration and frequency. A useful feature in audacity is the ability for the audio to be exported in various formats such as mp3, wav etc. An example of generating a sine wave of 1kHz is shown in, Figure \ref{fig: audacity}.
\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 1\textwidth]{../Figures/audacity_1}
		\caption{Generating a 1kHz sine wave in audacity}
		\label{fig: audacity}
	\end{centering}
\end{figure}


\section{HDL Design}\label{sec: DD HDL Design}
Most of the designs and experimentation revolve around the use of the DE0-Nano board. Therefore, the all the programming for this section has been done using Intel's Quartus Prime 20.1 Software.

\subsection{Experiment 1 - HS1 - Mic-amp system} \label{subsec: DD HL experiment 1}

\subsubsection{Experiment Setup}
To meet requirement HR1 and specification HS1 detailed in tables \ref{tbl: HDL proj req} and \ref{tbl: HDL specs}, a simple experiment utilising a power source, multimeter, loud speaker and the mic-amp IC can be designed. For the experiment, simply connect the \textit{VCC} and \textit{GND} pins of the mic-amp IC to the power supply. Next connect the multimeter to the output pin of the IC. An example of this experimental setup can be seen in Figure \ref{fig: experiment 1}

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 0.6\textwidth]{../Figures/experiment_1}
		\caption{Experiment 1 setup}
		\label{fig: experiment 1}
	\end{centering}
\end{figure}

\subsubsection{Experiment Procedure and Expectations}

In a quiet room it can be expected that the output reading from the mic-amp should be approximately \textit{1.25V} which is the offset value as per the data sheet \cite{MAX9814}. For the next test, place a loud audio source (speaker) near the microphones- the multimeter should have readings of approximately \textit{2V} - the maximum output from the mic. An audio source at reasonable loudness (approximately 50-60 dB),  would result in the value of the multimeter constantly fluctuating between \textit{1.25V} and \textit{2V}

 
\subsection{Experiment 2 - HS2 - ADC system} \label{subsec: DD HL experiment 2}

\subsubsection{Experiment Setup}
After confirming that the Amp-mic IC is working. The next experiment would be to interface the ADC with the IC. For this, what is required is an audio source outputting an audio signal of a known frequency, the mic-amp IC, the DE0-Nano's on board ADC and a signal analysing device such as an oscilloscope or software that is able to replicate that functionality. The setup used in Figure \ref{fig: setup} is used for the remainder of the experiments for the HDL implementation. Black is the \textit{GND} connection, Red the \textit{VCC = 3.3V}, Yellow is the \textit{Gain} control set to \textit{VCC} to tie gain at \textit{40dB}, and  Blue is the \textit{Output} from the mic-amp into \textit{Channel 0} of the FPGA ADC.

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 0.8\textwidth]{../Figures/setup}
		\caption{Setup used for Experiments 2 - 6}
		\label{fig: setup}
	\end{centering}
\end{figure}


\subsubsection{Experiment Procedure and Expectations}
One the setup is complete, the next step would be to code the FPGA to utilise the on board ADC. For this there are 2 options. The easier method is to utilise Intel's provided ADC IP core \cite{Intel2017ADC}. Alternatively, for more understanding and customisability, one could write an SPI Interface. The code used for the SPI Interface can be found in Appendex \ref{app: subsec: SPI_interface}. In either implementation, one important factor to consider when taking samples from the ADC, is the clock speed. The ADC has a recommended optimal frequency range of 0.8MHz - 3.2MHz. One the ADC has been correctly setup,  apply a signal of known frequency to the mic-amp IC.  Using an oscilloscope or software like Intel's Signal Tap, measure the output from the ADC ensuring the correct channel has been selected. Finally compare the reading into the FPGA to the audio signal source.


\subsection{Experiment 3 - HS3 - FFT Codes} \label{subsec: DD HL experiment 3}
There are various available FFT codes in both verilog and VHDL available online (especially on Github). Due to various factors such as time constraints and reliability, Intel's FFT IP core \cite{Intel2017FFTCore} was chosen. Although it requires a licence to be used commercially, it is a reliable, easy to implement and highly configurable core. Furthermore, this core has been optimised by the developers to produce a highly efficient FFT, which makes it an ideal FFT core for this investigation.

This core makes use of the radix-2 and radix-4, mixed radix-4/2 algorithms for fixed and floating point FFTs respectively. In this case, the core should be configured to the floating point FFT. These algorithms have been discussed in Chapter \ref{Chap: Lit_ Review}, Section \ref{Sec: ch2 FFTs}. The ideal configuration for this application is a 1024-point FFT, a 12-bit input (from the ADC) and a 23-bit output (maximum length allowed).

The important parameters to look for in the implementation of this core are discussed in table \ref{tbl: fft parameters}

\begin{table}[h!]
	\centering{%
		\begin{tabular}{|l|l|}
			\hline 
			\textbf{Signal Name} & \textbf{Description} \\ \hline \hline
			\textbf{sink\_imag}			& Imaginary part of the input signal. Set to 0 in this case \\ \hline
			\textbf{sink\_real}			& Real part of the input signal - a 12 bit value from ADC \\ \hline
			\textbf{sink\_sop}			& Indicates the first sample for FFT \\ \hline
			\textbf{sink\_eop} 			& Indicates final sample in FFT frame\\ \hline
			\textbf{sink\_valid} 		& Indicates that data on bus is valid and is set to 1 in this case \\ \hline
			\textbf{source\_ready}		& Set to 1 to indicate ready to accept data \\ \hline
			\textbf{source\_valid}		& Set to 1 to indicate valid data to output \\ \hline
			\textbf{source\_real}		& Real values output from FFT - 23-bit \\ \hline
			\textbf{source\_imag}		& Values of  imaginary output from the FFT - 23-bit \\ \hline
			\textbf{fftpts\_in}			& Number of FFT points per frame. This is set to 1024. \\ \hline
			\textbf{inverse}			& Set to 0 for FFT and 1 for inverse FFT \\ \hline
		\end{tabular}%
	}
	\caption{Table of important parameters for Intel's FFT IP Core}\label{tbl: fft parameters}
\end{table}

All the \textbf{sink} values are inputs to the FFT core, whereas all \textbf{source} values are outputs. The \textbf{sink\_sop} and \textbf{sink\_eop} are separated by 1024 points in this case.

\subsection{Experiment 4 - HS4 - Implementing FFT} \label{subsec: DD HL experiment 4}

\subsubsection{Experiment Setup}
As discussed in the previous section, Intel's IP FFT core is to be used. The input into the FFT should be the data stream from the ADC as described in Experiment 2 \ref{subsec: DD HL experiment 2}. The combination of the Experiments 2 and 3 are what make up Experiment 4. In order to make sure that the ADC samples at the correct speed, a Phase Locked Loop (PLL) - which is used to change the clock speed, should be implemented to transform the on-board clock speed from 50 MHz to 3.2 MHz. The setup used in this configurations can be seen in Figure \ref{fig: fft_block_diagram}.

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 1\textwidth]{../Figures/FFT_implementation.jpg}
		\caption{Experimental Setup for Implementing an FFT on the Audio Stream}
		\label{fig: fft_block_diagram}
	\end{centering}
\end{figure}

In order to ensure the proper utilisation of the FFT IP core, a wrapper module, \textit{control\_for\_fft} is created. This module is responsible for setting the constant values such as \textit{ready, valid, fftpts\_in} as well as the important \textit{ Start Of Packet (SOP) and End Of Packet (EOP)} values. These values are utilised by the FFT IP core and the output goes to \textit{real\_power} and \textit{source\_real}. The code for this Experiment can be seen in Appendix \ref{app: subsec: fft on adc}


\subsubsection{Experiment Procedure and Expectations}
Once the experiment has been setup as described above, a similar approach to the previous Experiments is taken. First, program the FPGA and reduce the offset of the mic-amp system. This can be done by running the ADC in a quiet place and reducing the value of the input by the DC offset read in. Once the offset has been reduced, utilise an audio source (such as a speaker) to play a signal of know frequency to the ADC. This signal should be between 20Hz - 20kHz. After this audio signal has been applied, if the above setup is utilised, the FFT should be performed on the signal from the ADC. The values read by the ADC and the FFT performed on those values can be seen using Signal Tap. For a better representation of the data from signal tap, Excel can be used on the values exported. It is expected that there should be multiple spikes on the FFT. This is due to the multiple \textit{sop} and \textit{eop} within the display of Signal Tap. Furthermore, if the signal is not directly on a bin on the frequency axis, it should be expected that there will be smearing of the  spike between bins.

\subsection{Experiment 5 - HS5 - Measuring Power Consumption} \label{subsec: DD HL experiment 5}
The culmination of all the Experiments 1- 3 is Experiment 4. Experiment 5 is used to analyse the effectiveness of Experiment 4. Using the same principle as Garrido \cite{Garrido2014} and the data provided by Quaruts,  equation \ref{eqn: FPGA_usage_quartus} can be used to determine the FPGA utilisation, $U$. This equation takes an average of the important limiting factors of FPGA resources, namely Logic Elements (LE), Memory Bits (MB), and Multiplier Elements (ME).

\begin{equation}\label{eqn: FPGA_usage_quartus}
U = \frac{(LE\%)+(MB\%)+(ME\%)}{3}
\end{equation}

%\textbf{MEASURE POWER CONSUMED!!!!!!!!!!!!!!}

\subsection{Experiment 6 - HS6 - Measuring the Speed and Ease of Implementation } \label{subsec: DD HL experiment 6}
Once the FFT has been programmed on the FPGA board, the process acts instantly on the signal provided. This means that there is a continuous FFT being applied. Therefore, the time taken for the program to be compiled and programmed on the board can be the factors considered for speed of implementation. The simple formula used to measure Speed of Implementation (SI) is shown in Equation \ref{eqn: speed of implementation}. The Speed of Execution (SE) of the calculation of the FFT, can be derived from the clock speed and latency as shown in equation \ref{eqn: speed of execution}.

\begin{equation}\label{eqn: speed of implementation}
SI = CompilationTime + BoardProgramTime	
\end{equation}

\begin{equation}\label{eqn: speed of execution}
	SI = ClockSpeed * FFTCalculationLatency	
\end{equation}

Of course for other factors may affect compilation time, the device on which all the programming will take place has the following specs: Ubuntu 20.04,Intel Core i7-8750H CPU @ 2.20GHz,  6 Cores, 12 Logical Processors, 16GB RAM, 256 SSD and GTX1050Ti graphics card.
The code should be compiled several times to ensure that the experiment takes place on an a warm cache. 

For ease of implementation, the best way to measure this, would be to list the factors affecting it and assign a percentage of simplicity. Thereafter, sum of the percentages and divide by the number of factors. The main factors that could be considered are:
\begin{enumerate}
	\item Board setup and features
	\item Available software and intuitiveness of it
	\item Programming language difficulty
	\item Documentation
\end{enumerate}
The result can then be spilt into categories: (0 - 33)\% - difficult to implement, (34 - 66)\% - average/ expected level of difficulty to implement and (67 - 100)\% easy to implement.

\section{RISC-V Design}\label{sec: DD RV Design}
In any engineering project, it is always good practice to first implement a design in a simulation and then afterwards practically. This project is no different. As per the requirements and specifications in Section \ref{sec: HDL requirements}, whatever is done in simulation is to be done on the FPGA afterwards.

\subsection{Experiment 1 - RS1 - Implementing FFT in C} \label{subsec: DD RV experiment 1}
There are 3 parts to this experiment. First, is to read in an audio signal, second, implement an FFT on a sine wave and, finally, is to perform the FFT on the audio signal.

\subsubsection{Reading audio signal}
There are various audio formats that can be used as audio input. In this case, because of lack of compression, resulting in ease of manipulation and reading, the wave (.wav) format was chosen. The important parts of a wave file can be seen in Figure \ref{fig: wave} \cite{wave}. 

\begin{figure}[h]	
	\begin{centering}
		\includegraphics[width = 0.5\textwidth]{../Figures/wave.png}
		\caption{Various important parts of a wave file}
		\label{fig: wave}
	\end{centering}
\end{figure}

There are several libraries, in C, that are available to read in an audio signal such as FFmpeg \cite{ffmpeg} and libsndfile \cite{lsbsndfile}. To ensure with certainty that the code implemented in C on my host computer would run on RISC-V, I decided to read the audio signal without the use of additional libraries. After some research I adapted some code available online \cite{read_wave} and shown in Appendix: \ref{app: subsec: wave file}.

\subsubsection{FFT in C}
There are several available FFT codes available online. Some of the more popular ones seem to be the FFTW \cite{FFTW05} and the KISS FFT \cite{KISSFFT}. For simplicity of implementation and size, simple radix-2 FFT code from Washington University in St. Louis's Professor, M.V. Wickerhauser was used \cite{fft_code}. This code has a $O(Nlog_2N)$ efficiency and is shown in Appendix \ref{app: subsec: fft}. Although not as efficient as the FFTW, the size and ease of implementation are worth the slight performance lost. 

The final part of this experiment is simply to combine the 2 previous parts. This is done easily due to Object Oriented Programming. All that is required is to parse the input audio signal as array into the FFT code. For ease of processing the data and verifying the experiment, it is also useful to export the output data as a .csv file.

\subsection{Experiment 2 - RS2 - Setup Qemu}\label{subsec: DD RV experiment 2 - qemu}
An ideal open source virtualiser and emulator for this application is Qemu \cite{qemu}. Although an extremely useful and powerful too, Qemu can be difficult to set up. Many tutorials online are either difficult to follow or outdated - even the official RISC-V document \cite{RISCV_getting_started}. With some assistance from Dr J. Wyngaard, utilising Debian \cite{debian}, the following steps were determined to be necessary to setup Qemu successfully: 
\begin{enumerate}
	\item Install requirements on Ubuntu 20.04. These include: \textit{debian-ports-archive-keyring, gcc-riscv64-linux-gnu, qemu-system-misc, opensbi, qemu-user-static, binfmt-support, debootstrap, libguestfs-tools} and \textit{u-boot-qemu\_2020.04+dfsg-2ubuntu\_all.deb}.
	\item Create a RISC-V filesystem, a Chroot. - this is a space on the local hard drive that looks like Linux. All the important libraries that are expected in the \textquotedblleft /root\textquotedblright are in this space, but for a different architecture and Operating System
	\item Configure the new filesystem. This is done by \textquotedblleft Logging in\textquotedblright and running commands to: 
	\begin{itemize}
		\item Setup a virtual network
		\item Set password for root
		\item Disable the \textit{getty} on \textit{hvc0} as \textit{hvc0} and \textit{ttyS0} share the same console device in Qemu
		\item Install kernel and bootloader infrastructure - \textit{linux-image-riscv64 u-boot-menu}
		\item Install and configure \textit{ntp} tools
		\item Configure syslinux-style boot menu
	\end{itemize}
	\item Turn the Chroot filesystem into a disk image
	\item Boot it and login with username: root and the password set in step 3
	\item Setup ssh and get IP
\end{enumerate}

If the above steps are followed, one should be able to ssh into the emulation. This can be tested by running the command : \textit{ssh yourname@localhost -p 22222} from another terminal. Another test to ensure that both the RISC-V core and Linux are being emulated successfully, is to compile a simple \textquotedblleft Hello, world!\textquotedblright program on the host PC. Compiling can be done with \textit{riscv64-linux-gnu-gcc -o mainRV helloWorld.c} and copying over to the emulation with \textit{scp -P 22222 mainRV yourname@localhost:}. Finally, the program can be run using the command \textit{./mainRV}.

\subsection{Experiment 3 - RS3 \& RS4 - Implementing FFT on Linux on RISC-V - Emulation}\label{subsec: DD RV experiment 3 - FFT in qemu}
To ensure that this investigation is fair, the same specifications that were used for HDL Experiments 3 and 4 (Sections \ref{subsec: DD HL experiment 3} and \ref{subsec: DD HL experiment 4}) are to be used in the RISC-V implementation. This means that a 1024-point FFT should be applied to a 6.25kHz sine wave.

One important factor to cognizant of, is that the RISC-V core used in this emulation, is a \textbf{RV64ACDFIMSU} core. This core is capable of floating point arithmetic. Most of the lightweight Linux capable RISC-V cores, however, do not include floating point arithmetic. This is mainly due to the amount of memory required for these complex operations. With this in mind, the FFT applied should be integer based. With slight modifications to the FFT code, both floating point and integer FFTs can be applied and compared.

Verification of the FFT can be done by checking that the spike in the FFT plot is in the correct frequency bin. This means that there should be a spike at 6.25kHz on the frequency axis or between the closest bins.

\subsection{Experiment 4 - RS5 \& RS6 - Implementing RISC-V Core and Linux on FPGA}\label{subsec: DD RV experiment 4 - RISCV on FPGA}
As discussed in \ref{Sec: ch2 Riscv}, the Github page \textit{Linux On Litex} \cite{LinunxOnLitex}, provides a step by step procedure to install both the Vexrisc core and Linux on an FPGA. In summary the steps to be followed are as follows:
\begin{enumerate}
	\item Install prerequisites: \textit{build-essential, device-tree-compiler, wget, git and python3-setuptools}
	\item Clone the \textit{linux-on-litex-vexriscv} repository
	\item Install Litex
	\item Install the RISC-V toolchain and add it to \textit{\$PATH}
	\item For a basic simulation install \textit{Verilator} and run the simulation
	\item Compile the bitstream for the DE0-Nano board
	\item Load the bitstream to the board
	\item Using the Litex tool \textit{lxterm} to install Linux on the Vexriscv Core through serial connection
\end{enumerate}

If all the steps go well, then there should be a terminal running showing that Linux is running on the FPGA. This terminal should look like the Simulation that can be run in Step 5 but with more available features.

\subsection{Experiment 5 - RS7 \& RS8 - Implementing FFT on Audio Stream on Linux, Vexriscv Core on FPGA}\label{subsec: DD RV experiment 5 - fft and adc}
For this experiment, a similar procedure to Experiments 1 and 2 is utilised. To implement the FFT on the Vexriscv core, the FFT code must be correctly compiled for a \textbf{RV32IMAC} core. The program and input audio file should be copied to the FPGA using serial communication or SSH if available. Once the FFT is successfully run on a wave file, the output .csv files should be copied back to the host PC and validated. 

If the FFT is working as expected, the next step would be to communicate with the on-board ADC, ADC128S022 \cite{Altera2012}. It may also be required that the Vexriscv core is edited such that the ADC pins on the FPGA board are enabled. This can be done by editing the pin allocation files (.pin and .psf) or by using Quartus. Once the pins are active, using a library for the ADC would probably be the easiest. Once the ADC is sampling a signal correctly - this can be checked using a potentiometer connected to the ADC input pin - then an audio signal can be played and the code can be modified to implement a real-time FFT.

%Getting the ADC to work will be time consuming and should be attempted only after majority of the project has been completed.

\subsection{Experiment 6 - RS9 - Resources Consumed and Ease of Implementation}\label{subsec: DD RV experiment 6 - resources and ease}
The speed, resources consumed and ease of implementation for both the Qemu emulation and FPGA implementation will be determined here. This experiment will follow the same procedure as Experiments 5 and 6 (Sections \ref{subsec: DD HL experiment 5} and \ref{subsec: DD HL experiment 6}) for the HDL implementation. The host computer is the same as before (see Section \ref{subsec: DD HL experiment 6} for specifications).

For the Qemu implementation, the time to start the emulation could be equated to the time taken to compile and flash the FPGA board with the RISC-V core and the Linux image. Other factors included for the Qemu implementation are cross-compilation time and execution time. The total time will be the sum of the time taken to compile and flash the RISC-V core, Linux and the time taken to compile the FFT program. The total execution time can be measured easily in C using the \textit{time.h} library and \textit{clock()} method.

The resources utilised can be measured using Quartus software and Equation \ref{eqn: FPGA_usage_quartus}. As in the HDL experiment, the 4 factors to be considered for both the emulation and FPGA implementation are: board setup, software and intuitiveness, programming language difficulty, available documentation, and resources.