module spectrum(CLOCK_50, KEY, LED, clk, sclk,din,dout,cs,count,dataout,clock_out,real_power,imag_power,sink_valid,sink_sop,sink_eop,fft_source_sop);

///////// Inputs and outputs /////////
input CLOCK_50;
input [1:0] KEY;
output reg [7:0] LED;

output clk; //clock out from the pll

/////////////////////// for ADC\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// input clk; been replaced with clock_50
output reg din;
output wire sclk;
input dout; 			//info coming out of adc into fgpa - series of bits
output reg cs;
output reg [11:0] dataout;
reg [11:0] data_temp;
output reg clock_out;

reg ADD2,ADD1,ADD0; // 010 = channel 2 , 000 = channel 0

output reg[3:0] count;

///////////////////////for FFT\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
output wire [22:0] real_power;
output wire [22:0] imag_power;
output wire sink_valid;
output wire sink_sop;
output wire sink_eop;
output wire fft_source_sop;

////////// TEST//////////
always @ (posedge CLOCK_50)
begin
LED <= 8'b00000000;
if(!KEY[0])
	LED <= 8'b01010101;
else if (!KEY[1])
	LED <= 8'b10101010;
end

// instantiation of pll
pll pll_instn (
	.inclk0(CLOCK_50),
	.c0(clk)
	);


initial
begin
	count = 4'd0;
	cs = 1;	
	ADD2 = 0;
	ADD1 = 0;
	ADD0 = 0;
	clock_out = 0;
	dataout = 12'd0;
	data_temp = 12'd0;
end


always@(negedge clk)
begin
	if (count == 1)
	begin
		cs <= 0;
	end
end

assign sclk = cs ? 1 : clk;


always@(posedge clk)
begin
	count <= count + 4'd1;
end

always @ (posedge clk)
begin
	case (count)
	3: begin
	din <= ADD2;
	end
	4: begin
	din <= ADD1;
	end
	5: begin
	din <= ADD0;
	end
	endcase
end


always @ (posedge clk)
begin
	case (count)
	4:			dataout <= data_temp;
	5:			data_temp[11] <= dout;
	6:			data_temp[10] <= dout;
	7:			data_temp[9] <= dout;
	8:			data_temp[8] <= dout;
	9:			data_temp[7] <= dout;
	10:		begin
					data_temp[6] <= dout;
					clock_out <= 1;					//clock goes high halfway through cycle
				end
	11:		begin										//sampling at same rate coming out of interface code
					data_temp[5] <= dout;
					clock_out <= 0;					
				end
	12:		data_temp[4] <= dout;
	13:		data_temp[3] <= dout;
	14:		data_temp[2] <= dout;
	15:		data_temp[1] <= dout;
	16:		data_temp[0] <= dout;
	endcase
end

//fft signals
wire [31:0] short_in_signal;
wire sink_ready;
wire [9:0] fft_pts;
wire fft_source_eop;
wire [11:0] real_to_fft_p;
wire [11:0] imag_to_fft_p;
reg [4:0] c;
reg reset_n;
reg eop2,sop2,eop5;

reg [11:0] dataout_offset;
//assign dataout_offset = dataout - 12'd1531;

// the following is to get a positive edge on the reset_n
// this is required for the fft to work properly

initial
begin
	dataout_offset = dataout;
	reset_n = 0;
	c = 5'd0;
end

//wait 10 clock cycles and then change reset_n value
always @ (posedge clk) //clock 50
begin
	dataout_offset = dataout - 12'd1531;
	c = c + 5'd1;
	if (c == 5'd10)
	begin
		reset_n =1;
	end
end


//wire [11:0] dataout_offset;
//assign dataout_offset = dataout - 12'd1531;


//instantiate control_for_fft function
control_for_fft control_for_fft_longer_inst(.clk(clk),.insignal(dataout_offset),.sink_valid(sink_valid),
.sink_ready(sink_ready),.sink_error(),.sink_sop(sink_sop),.inverse(inverse),.outreal(real_to_fft_p),.outimag(imag_to_fft_p),.fft_pts(fft_pts));


//instantiation of fft

fft fft_inst(
	.clk(clk),
	.reset_n(reset_n),
	.sink_valid(sink_valid),
	.sink_ready(sink_ready),
	.sink_error(2'b00),
	.sink_sop(sink_sop),
	.sink_eop(sink_eop),
	.sink_real(real_to_fft_p),
	.sink_imag(imag_to_fft_p),
	.fftpts_in(fft_pts),
	.inverse(1'b0),
	.source_valid(),
	.source_ready(1'b1),
	.source_error(),
	.source_sop(fft_source_sop),
	.source_eop(fft_source_eop),
	.source_real(real_power),
	.source_imag(imag_power),
	.fftpts_out()
);

endmodule
