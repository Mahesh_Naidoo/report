module spi_interface(clk,sclk,din,dout,cs,count,dataout,clock_out,LED);

input clk;
output reg din;
output wire sclk;
input dout; 			//info coming out of adc into fgpa - series of bits
output reg cs;
output reg [11:0] dataout;
reg [11:0] data_temp;
output reg clock_out;

reg[7:0] led_temp;
output reg [7:0] LED;


reg ADD2,ADD1,ADD0; // 010 = channel 2 , 000 = channel 0

output reg[3:0] count;

initial
begin
	count = 4'd0;
	cs = 1;	
	ADD2 = 0;
	ADD1 = 1;
	ADD0 = 0;
	clock_out = 0;
	dataout = 12'd0;
	data_temp = 12'd0;
	LED = 8'd0;
	led_temp = 8'd0;
end


always@(negedge clk)
begin
	if (count == 1)
	begin
		cs <= 0;
	end
end

assign sclk = cs ? 1 : clk;


always@(posedge clk)
begin
	count <= count + 4'd1;
end

always @ (posedge clk)
begin
	case (count)
	3: begin
	din <= ADD2;
	end
	4: begin
	din <= ADD1;
	end
	5: begin
	din <= ADD0;
	end
	endcase
end

always @ (posedge clk)
begin
	LED <= (dataout>>4); 
end


always @ (posedge clk)
begin
	case (count)
	4:			dataout <= data_temp;
	5:			data_temp[11] <= dout;
	6:			data_temp[10] <= dout;
	7:			data_temp[9] <= dout;
	8:			data_temp[8] <= dout;
	9:			data_temp[7] <= dout;
	10:		begin
					data_temp[6] <= dout;
					clock_out <= 1;					//clock goes high halfway through cycle
				end
	11:		begin										//sampling at same rate coming out of interface code
					data_temp[5] <= dout;
					clock_out <= 0;					//
				end
	12:		data_temp[4] <= dout;
	13:		data_temp[3] <= dout;
	14:		data_temp[2] <= dout;
	15:		data_temp[1] <= dout;
	16:		data_temp[0] <= dout;
	endcase
end

endmodule
